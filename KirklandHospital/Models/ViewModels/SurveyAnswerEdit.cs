﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KirklandHospital.Models.ViewModels
{
    public class SurveyAnswerEdit
    {
        public SurveyAnswerEdit()
        {

        }
        public virtual SurveyAnswer SurveyAnswer { get; set; }

        public IEnumerable<SurveyQuestion> SurveyQuestions { get; set; }

 

    }
}
