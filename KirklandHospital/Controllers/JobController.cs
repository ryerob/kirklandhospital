﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using KirklandHospital.Models;
using KirklandHospital.Data;
using KirklandHospital.Models.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace KirklandHospital.Controllers
{
    public class JobController : Controller
    {
        private readonly KirklandDBContext db;

        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public JobController(KirklandDBContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult>List(int pagenum)
        {

            var _jobs = await db.Jobs.Include(j => j.Department).ToListAsync();
            int jobcount = _jobs.Count();
            int perpage = 5;
            int maxpage = (int)Math.Ceiling((decimal)jobcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<Job> jobs = await db.Jobs.Include(j => j.Department).Skip(start).Take(perpage).ToListAsync();     

            var user = await GetCurrentUserAsync();

            if (user != null)
            {
                if (user.AdminID == null)
                {
                    ViewData["UserHasAdmin"] = "False";
                }
                else
                {
                    ViewData["UserHasAdmin"] = "True";
                }
            }

            return View(jobs);
            //return View(await db.Jobs.ToListAsync());
        }

        public async Task <ActionResult>Create()
        {

            var user = await GetCurrentUserAsync();

            if (user == null)
            {
                return BadRequest();
            }

            if (user.AdminID == null)
            {
                return Forbid();
            }
         



            JobDepartment jobdepartmentview = new JobDepartment();

            jobdepartmentview.Departments = db.Departments.ToList();
            return View(jobdepartmentview);
        }

        
        public ActionResult Edit(int? id)
        {
            
            JobDepartment jobdepartmentview = new JobDepartment();

            jobdepartmentview.Departments = db.Departments.ToList();
            jobdepartmentview.Job = db.Jobs.Find(id);
            return View(jobdepartmentview);
        }

      
        [HttpPost]
        public ActionResult Create(string Title_new, string Description_new, DateTime PostDate_new,
          DateTime ClosingDate_new, string Requirements_new, string Email_new, int Department_new, string Application_new, Job job)
        {
            var current_date = DateTime.Now;
            job.JobPostDate = current_date;

            string query = "insert into jobs (JobTitle, JobDescription, JobPostDate, JobClosingDate," +
                "JobRequirements, JobContactEmail, DepartmentID, JobApplicationRequirements)" +
                "values (@title, @description, @postdate, @closingdate, @requirements, @email, @department," +
                "@application)";
            SqlParameter[] myparams =
            {
                new SqlParameter("@title", Title_new),
                new SqlParameter("@description", Description_new),
                new SqlParameter("@postdate", job.JobPostDate),
                new SqlParameter("@closingdate", ClosingDate_new),
                new SqlParameter("@requirements", Requirements_new),
                new SqlParameter("@email", Email_new),
                new SqlParameter("@department", Department_new),
                new SqlParameter("@application", Application_new)
            };
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");

        }

        //Edit isn't working - need to fix
        [HttpPost]
        public ActionResult Edit(int id, string Title, string Description,
         DateTime ClosingDate, string Requirements, string Email, int Department, string Application)
        {
            

            string query = "update jobs set JobTitle = @title, JobDescription = @description, JobClosingDate =  @closingdate," +
                "JobRequirements = @requirements, JobContactEmail = @email, DepartmentID = @department, JobApplicationRequirements= @application " +
                "where jobid = @id" 
               ;
            SqlParameter[] myparams =
            {
                new SqlParameter("@title", Title),
                new SqlParameter("@description", Description),
                new SqlParameter("@closingdate", ClosingDate),
                new SqlParameter("@requirements", Requirements),
                new SqlParameter("@email", Email),
                new SqlParameter("@department", Department),
                new SqlParameter("@application", Application),
                new SqlParameter("@id", id)

            };
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");

        }

        public async Task<ActionResult> Delete(int id)
        {
            var user = await GetCurrentUserAsync();

            if (user == null)
            {
                return BadRequest();
            }

            if (user.AdminID == null)
            {
                return Forbid();
            }

            //query to delete an object from the database where the object matches the id passed through
            string query = "delete from jobs where JobID = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }

        public ActionResult Show(int id)
        {
            Job job = new Job();
            job = db.Jobs.Find(id);
            return View(job);
        }



    }
}