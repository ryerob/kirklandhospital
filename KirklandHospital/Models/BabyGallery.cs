﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KirklandHospital.Models
{
    public class BabyGallery
    {
        [Key]
        public int PostID { get; set; }

        [Required, StringLength(255), Display(Name = "Post Title")]
        public string PostTitle { get; set; }

        [Required, StringLength(int.MaxValue), Display(Name = "Post Content")]
        public string PostContent { get; set; }

        [Required, StringLength(255), Display(Name = "Family Name")]
        public string FamilyName { get; set; }

        [DataType(DataType.Date), Display(Name = "Birth Date")]
        public string BirthDate { get; set; }

        [Required]
        public string ImagePath { get; set; }
    }
}
