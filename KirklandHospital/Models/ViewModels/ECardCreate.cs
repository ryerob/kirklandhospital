﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KirklandHospital.Models.ViewModels
{
    public class ECardCreate
    {
        public ECardCreate()
        {

        }



        public virtual ECard ECard { get; set; }

        public IEnumerable<ECardMessage> ECardMessages { get; set; }
        public IEnumerable<ECardTemplate> ECardTemplates { get; set; }

    }
}