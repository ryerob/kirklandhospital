﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using KirklandHospital.Data;
using KirklandHospital.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KirklandHospital.Controllers
{
    public class BookAnAppointmentController : Controller
    {
        private readonly KirklandDBContext db;

        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public IActionResult Index()
        {
            return View();
        }
        public BookAnAppointmentController(KirklandDBContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //check if the user is admin or not
            //UserState
            //0 => Public User
            //1 => User but no admin
            //2=> User admin
            if (user == null) return 0;
            var userid = user.Id;
            if (user.AdminID == null) return 1;
            else
            {
                return 2;
            }
        }


        public async Task<ActionResult> List(int pagenum)
        {
            //three situations
            //0=>forbid()
            //1=>only that person's appointments who booked
            //2=>all appointments list

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

            if (userstate == 0) return Forbid();

            if (userstate == 1)
            {
                var _appointments = await db.BookAnAppointments.Where(ap=>ap.userid == user.Id).ToListAsync();
                int appointmentcount = _appointments.Count();
                int perpage = 5;
                int maxpage = (int)Math.Ceiling((decimal)appointmentcount / perpage) - 1;
                if (maxpage < 0) maxpage = 0;
                if (pagenum < 0) pagenum = 0;
                if (pagenum > maxpage) pagenum = maxpage;
                int start = perpage * pagenum;
                ViewData["pagenum"] = (int)pagenum;
                ViewData["PaginationSummary"] = "";
                if (maxpage > 0)
                {
                    ViewData["PaginationSummary"] =
                        (pagenum + 1).ToString() + " of " +
                        (maxpage + 1).ToString();
                }
                //two different queries here like line 61-70
                //one for userstate == 1 and one for userstate ==2
                List<BookAnAppointment> bookanappointments = await db.BookAnAppointments.Where(ap => ap.userid == user.Id).Skip(start).Take(perpage).ToListAsync();
                return View(bookanappointments);
            }
            else if (userstate == 2)
            {
                var _appointments = await db.BookAnAppointments.ToListAsync();
                int appointmentcount = _appointments.Count();
                int perpage = 5;
                int maxpage = (int)Math.Ceiling((decimal)appointmentcount / perpage) - 1;
                if (maxpage < 0) maxpage = 0;
                if (pagenum < 0) pagenum = 0;
                if (pagenum > maxpage) pagenum = maxpage;
                int start = perpage * pagenum;
                ViewData["pagenum"] = (int)pagenum;
                ViewData["PaginationSummary"] = "";
                if (maxpage > 0)
                {
                    ViewData["PaginationSummary"] =
                        (pagenum + 1).ToString() + " of " +
                        (maxpage + 1).ToString();
                }
                //two different queries here like line 61-70
                //one for userstate == 1 and one for userstate ==2
                List<BookAnAppointment> bookanappointments = await db.BookAnAppointments.Skip(start).Take(perpage).ToListAsync();
                return View(bookanappointments);
            }else
                return Forbid();
        }

        public async Task<ActionResult> New()
        {
           //User should have account to book an appointment
           
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            
            //userstate gets the context of the user
            switch (userstate)
            {
                case 0: return RedirectToAction("Register", "Account");
            }
            
            BookAnAppointment bookappointment = new BookAnAppointment();
            
            return View(bookappointment);
        }


        [HttpPost]
        public async Task<ActionResult> New(string FirstName_New, string LastName_New, string Address_New, string City_New, 
            string Province_New, string Postal_New, string Phone_New, string Email_New, string Gender_New, string DOB_New, 
            string AppointmentDate_New, string AppointmentTime_New)
        {
            //map userid of current logged in user
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

            if (userstate == 0) return Forbid();


            string query = "insert into bookanappointments (userid,PatientFName, PatientLName, PatientAddress, " +
                "PatientCity, PatientProvince, PatientPostalCode, PatientPhone, PatientEmail, PatientGender, "+
                "PatientDOB, AppointmentDate, AppointmentTime)" +
                " values (@uid, @fname, @lname, @adress, @city, @province, @postal,"+
                " @phone, @email, @gender, @dob, @appointmentdate, @appointmenttime)";
            SqlParameter[] myparams = new SqlParameter[13];
            myparams[0] = new SqlParameter("@uid", user.Id);
            myparams[1] = new SqlParameter("@fname", FirstName_New);
            myparams[2] = new SqlParameter("@lname", LastName_New);
            myparams[3] = new SqlParameter("@adress", Address_New);
            myparams[4] = new SqlParameter("@city", City_New);
            myparams[5] = new SqlParameter("@province", Province_New);
            myparams[6] = new SqlParameter("@postal", Postal_New);
            myparams[7] = new SqlParameter("@phone", Phone_New);
            myparams[8] = new SqlParameter("@email", Email_New);
            myparams[9] = new SqlParameter("@gender", Gender_New);
            myparams[10] = new SqlParameter("@dob", DOB_New);
            myparams[11] = new SqlParameter("@appointmentdate", AppointmentDate_New);
            myparams[12] = new SqlParameter("@appointmenttime", AppointmentTime_New);
           

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }

        public ActionResult Delete(int id)
        {
            string query = "delete from bookanappointments where bookanappointmentID = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }

        public ActionResult Edit(int id)
        {
            BookAnAppointment appointmentedit = new BookAnAppointment();
            appointmentedit = db.BookAnAppointments.Find(id);

            return View(appointmentedit);
        }

        [HttpPost]
        public ActionResult Edit(int? id, string FirstName, string LastName, string Address, string City,
            string Province, string PostalCode, string Phone, string Email, string Gender, string DOB,
            string AppointmentDate, string AppointmentTime)
        {
            if ((id == null) || (db.BookAnAppointments.Find(id) == null))
            {
                return NotFound();
            }

            string query = "update bookanappointments set PatientFName = @fname, PatientLName = @lname, PatientAddress = @address, " +
                "PatientCity = @city, PatientProvince = @province, PatientPostalCode = @postalcode, PatientPhone = @phone, PatientEmail = @email, " +
                " PatientGender = @gender, PatientDOB = @dob, AppointmentDate = @adate, AppointmentTime = @appointmenttime where bookanappointmentID = @id ";
            SqlParameter[] myparams = {
                new SqlParameter("@fname", FirstName),
                new SqlParameter("@lname", LastName),
                new SqlParameter("@address", Address),
                new SqlParameter("@city", City),
                new SqlParameter("@province", Province),
                new SqlParameter("@postalcode", PostalCode),
                new SqlParameter("@phone", Phone),
                new SqlParameter("@email", Email),
                new SqlParameter("@gender", Gender),
                new SqlParameter("@dob", DOB),
                new SqlParameter("@adate", AppointmentDate),
                new SqlParameter("@appointmenttime", AppointmentTime),
                new SqlParameter("@id", id)
            };

            db.Database.ExecuteSqlCommand(query, myparams);
            return RedirectToAction("List");

        }

    }
}