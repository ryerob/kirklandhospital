﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using KirklandHospital.Models;
using KirklandHospital.Data;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using KirklandHospital.Models.ViewModels;
using System;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace KirklandHospital.Controllers
{
    public class GiftShopProductController : Controller
    {//added IhostingEnvironment 30-34
        private readonly IHostingEnvironment _env;
        private readonly KirklandDBContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);
        public GiftShopProductController(KirklandDBContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        public async Task<ActionResult> List(int pagenumber)
        {
            var _products = await db.GiftShopProducts.ToListAsync();
            int productcount = _products.Count();
            int perpage = 7;
            int maxpage = (int)Math.Ceiling((decimal)productcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenumber < 0) pagenumber = 0;
            if (pagenumber > maxpage) pagenumber = maxpage;
            int start = perpage * pagenumber;
            ViewData["pagenumber"] = (int)pagenumber;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenumber + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

           
            List<GiftShopProduct> GiftShopProducts = await db.GiftShopProducts.Skip(start).Take(perpage).ToListAsync();
            
            //split views: Admin can see the table with all data including id of products
            string qstAdmin = "SELECT * FROM GiftShopProducts";
            IEnumerable<GiftShopProduct> giftShopProductsAdmin = db.GiftShopProducts.FromSql(qstAdmin);
           
            // and User is presented by a view of products with partial data(no product id)
            string qst = "SELECT ProductID, ProductName, ProductDescription, ProductPrice, ProductQuantity, ProductImage from GiftShopProducts"; 
            IEnumerable<GiftShopProduct> giftShopProducts = db.GiftShopProducts.FromSql(qst);
            var user = await GetCurrentUserAsync();
          
            if (user != null)
            {
                if (user.AdminID == null)
                {
                    ViewData["IsAdmin"] = "False";
                    return Forbid();
                }
                else
                {
                    ViewData["IsAdmin"] = user.AdminID.ToString();
                }

                return View(giftShopProductsAdmin);
            }
            else
            {

                ViewData["IsAdmin"] = "None";
                return View(giftShopProducts);
            }
            //return View(db.GiftShopProducts.ToList());
        }

        public async Task<ActionResult> Create(int id)
        {
           var user = await GetCurrentUserAsync();

            if (user == null)
            {
                return BadRequest();
            }

            if (user.AdminID == null)
            {
                return Forbid();
            }

            return View();
        }
        [HttpPost]
        
        public ActionResult Create(string ProductName_New, string ProductDescription_New, int ProductPrice_New, int ProductQuantity_New, IFormFile ProductImage_New, GiftShopProduct GiftShopProduct)
        {
            //add image need to validate against extention .jpg, jpeg, png
            if (ProductImage_New != null)
            {
                string imgName = Path.GetFileName(ProductImage_New.FileName);
                string imgPath = Path.Combine(_env.WebRootPath, "images/Product");
                imgPath = Path.Combine(imgPath, imgName);
                ProductImage_New.CopyTo(new FileStream(imgPath, FileMode.Create));

               
                GiftShopProduct.ProductImage = Path.GetFileName(imgPath);


                string qst = "INSERT into GiftShopProducts (ProductName, ProductDescription, ProductPrice, ProductQuantity, ProductImage) values (@name, @description, @price, @quantity, @image)";


                SqlParameter[] parameters = {
                //@name parameter
                new SqlParameter("@name", ProductName_New),
                //@description parameter
                new SqlParameter("@description", ProductDescription_New),
                //@price parameter
                new SqlParameter("@price", ProductPrice_New),
                 //@quantity parameter
                new SqlParameter("@quantity", ProductQuantity_New),
                //@image parameter
                new SqlParameter("@image", GiftShopProduct.ProductImage)
                };

                db.Database.ExecuteSqlCommand(qst, parameters);
            }
            //GOTO: 
            return RedirectToAction("List");
        }


        //edit needs to be fixed: mapping to image path
        public async Task<ActionResult> Edit(int? id)
        {
            var user = await GetCurrentUserAsync();

            if (user == null)
            {
                return BadRequest();
            }

            if (user.AdminID == null)
            {
                return Forbid();
            }

           // return View();
            return View(db.GiftShopProducts.Find(id));
        }
        [HttpPost]
        public ActionResult Edit(int? id, string ProductName, string ProductDescription, int ProductPrice, int ProductQuantity, IFormFile ProductImage_New, GiftShopProduct GiftShopProduct)
        {
            if ((id == null) || (db.GiftShopProducts.Find(id) == null))
            {
                return NotFound();
            }


            if (ProductImage_New != null)
            {
                if (ProductImage_New.Length > 0)
                {
                    string imgName = Path.GetFileName(ProductImage_New.FileName);
                    string imgPath = Path.Combine(_env.WebRootPath, "images/Product");
                    imgPath = Path.Combine(imgPath, imgName);
                    ProductImage_New.CopyTo(new FileStream(imgPath, FileMode.Create));


                    GiftShopProduct.ProductImage = Path.GetFileName(imgPath);

                }
            }
                string qst = "UPDATE GiftShopProducts SET ProductName = @name, ProductDescription = @description, ProductPrice = @price" +
                "ProductQuantity = @quantity, ProductImage_New = @image WHERE ProductID = @id ";
                SqlParameter[] parameters = {
                new SqlParameter("@name", ProductName),
                new SqlParameter("@description", ProductDescription),
                new SqlParameter("@price", ProductPrice),
                new SqlParameter("@quantity", ProductQuantity),
                new SqlParameter("@image", GiftShopProduct.ProductImage),
                new SqlParameter("@id", id)
            };

                db.Database.ExecuteSqlCommand(qst, parameters);
 
            return RedirectToAction("List");


        }

        //no need to delete from customers table since the products in customer's table have been already bought
        public async Task<ActionResult> Delete(int id)
        {
            var user = await GetCurrentUserAsync();

            if (user == null)
            {
                return BadRequest();
            }

            if (user.AdminID == null)
            {
                return Forbid();
            }

            string qst = "DELETE FROM GiftShopProducts WHERE ProductID = @id";
            db.Database.ExecuteSqlCommand(qst, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }
         

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}