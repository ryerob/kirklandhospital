﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KirklandHospital.Models.ViewModels
{
    public class GiftShopCustomerEdit
    {
        
        public GiftShopCustomerEdit()
        {

        }

        public virtual GiftShopCustomer GiftShopCustomer { get; set; }

        public IEnumerable<GiftShopProduct> GiftShopProducts { get; set; }
    }
}

