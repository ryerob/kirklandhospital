﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KirklandHospital.Models
{
    public class Survey
    {
        [Key]
        public int SurveyID { get; set; }

        [Required, StringLength(255), Display(Name = "Date")]
        public DateTime Date { get; set; }

        [InverseProperty("Survey")]
        public virtual List<SurveyAnswer> SurveyAnswers { get; set; }
    }
}
