﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KirklandHospital.Models.ViewModels
{
    public class ECardView
    {
        public ECardView()
        {

        }



        public virtual ECard ECard { get; set; }

        public virtual ECardMessage ECardMessage { get; set; }
        public virtual ECardTemplate ECardTemplates { get; set; }

    }
}