﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using KirklandHospital.Data;
using KirklandHospital.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KirklandHospital.Controllers
{
    public class NewsEventController : Controller
    {
        private readonly KirklandDBContext db;
        private readonly IHostingEnvironment _env;
        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);


        public NewsEventController(KirklandDBContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        public IActionResult Index()
        {
            return View();
        }

        /*public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //check if the user is admin or not
            //UserState
            //0 => Public User
            //1 => User but no admin
            //2=> User admin
            if (user == null) return 0;
            var userid = user.Id;
            if (user.AdminID == null) return 1;
            else
            {
                return 2;
            }
        }*/
        
        public async Task<ActionResult> List(int pagenum)
        {

            List<NewsEvent> _newsevents = db.NewsEvents.ToList();
            /* Pagination */
            int newscount = _newsevents.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)newscount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<NewsEvent> newsevents = await db.NewsEvents.Skip(start).Take(perpage).ToListAsync();

            var user = await GetCurrentUserAsync();

            if (user != null)
            {
                if (user.AdminID == null)
                {
                    ViewData["UserHasAdmin"] = "False";
                }
                else
                {
                    ViewData["UserHasAdmin"] = "True";
                }
            }
            
            return View(newsevents);
        }

        /*public ActionResult List()
        {


            return View(db.NewsEvents.ToList());
        }*/

        public async Task<ActionResult> New()
        {

            var user = await GetCurrentUserAsync();

            if (user == null)
            {
                return BadRequest();
            }

            if (user.AdminID == null)
            {
                return Forbid();
            }
            

            NewsEvent newsview = new NewsEvent();
            return View(newsview);
        }
        [HttpPost]
        public ActionResult New(string NewsEventTitle_New, string NewsEventDescription_New, IFormFile img, NewsEvent NewsEvent)
        {


            var webRoot = _env.WebRootPath;
            if (img != null)
            {
                //checking the extension and extracting that from the image
                var valtypes = new[] { "jpeg", "jpg", "png", "gif" };
                var extension = Path.GetExtension(img.FileName).Substring(1);

                if (valtypes.Contains(extension))
                {
                    //giving each image a unique name based on date time and appending the extension
                    string imgtitle = DateTime.Now.ToString("yyyymmddMMss") + "." + extension;
                    string path = Path.Combine(webRoot, "images/NewsEvent");
                    path = Path.Combine(path, imgtitle);

                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        img.CopyTo(stream);
                    }

                    //Saving just the filename in the database, combine with the full path in the view to display
                    NewsEvent.ImagePath = Path.GetFileName(path);
                }


                string query = "insert into newsevents (NewsEventTitle, NewsEventDescription, ImagePath)" +
                " values (@title, @description, @imagepath)";
                SqlParameter[] myparams = new SqlParameter[3];
                myparams[0] = new SqlParameter("@title", NewsEventTitle_New);
                myparams[1] = new SqlParameter("@description", NewsEventDescription_New);
                myparams[2] = new SqlParameter("@imagepath", NewsEvent.ImagePath);

                db.Database.ExecuteSqlCommand(query, myparams);


            }
            return RedirectToAction("List");
        }

        public async Task<ActionResult> Delete(int id)
        {
            var user = await GetCurrentUserAsync();

            if (user == null)
            {
                return BadRequest();
            }

            if (user.AdminID == null)
            {
                return Forbid();
            }

            string query = "delete from newsevents where newseventID = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }



        public async Task<ActionResult> Edit(int id)
        {
            NewsEvent newseventedit = new NewsEvent();
            newseventedit = db.NewsEvents.Find(id);

            return View(newseventedit);
        }

        [HttpPost]
        public ActionResult Edit(int? id, string Title, string Description, IFormFile img, NewsEvent NewsEvent)
        {
            var webRoot = _env.WebRootPath;
            if (img != null)
            {
                //checking the extension and extracting that from the image
                var valtypes = new[] { "jpeg", "jpg", "png", "gif" };
                var extension = Path.GetExtension(img.FileName).Substring(1);

                if (valtypes.Contains(extension))
                {
                    //giving each image a unique name based on date time and appending the extension
                    string imgtitle = DateTime.Now.ToString("yyyymmddMMss") + "." + extension;
                    string path = Path.Combine(webRoot, "images/NewsEvent");
                    path = Path.Combine(path, imgtitle);

                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        img.CopyTo(stream);
                    }

                    //Saving just the filename in the database, combine with the full path in the view to display
                    NewsEvent.ImagePath = Path.GetFileName(path);
                }


                if ((id == null) || (db.NewsEvents.Find(id) == null))
                {
                    return NotFound();
                }

                string query = "update newsevents set NewsEventTitle = @title, NewsEventDescription = @description, ImagePath = @image, " +
                    " where NewsEventID = @id ";
                SqlParameter[] myparams = {
                new SqlParameter("@title", Title),
                new SqlParameter("@description", Description),
                new SqlParameter("@image", NewsEvent.ImagePath),
                new SqlParameter("id", @id)
                };

                db.Database.ExecuteSqlCommand(query, myparams);
                

            }
            return RedirectToAction("List");
        }

        public ActionResult Show(int id)
        {
            NewsEvent newsevent = new NewsEvent();
            newsevent = db.NewsEvents.Find(id);

            return View(newsevent);
        }
    }
}