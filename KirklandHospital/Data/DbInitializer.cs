﻿using KirklandHospital.Models;
using System;
using System.Linq;

namespace KirklandHospital.Data
{
    public static class DbInitializer
    {
        public static void Initialize(KirklandDBContext context)
        {
            
            context.Database.EnsureCreated();

            return;
        }
    }
}