﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using KirklandHospital.Data;
using KirklandHospital.Models;
using KirklandHospital.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KirklandHospital.Controllers
{
    public class SurveyAnswerController : Controller
    {
        private readonly KirklandDBContext db;
        public IActionResult Index()
        {
            return View();
        }
        public SurveyAnswerController(KirklandDBContext context)
        {
            db = context;
        }
        public async Task<ActionResult> List(int pagenum)
        {
            List<SurveyAnswer> _surveyanswers = db.SurveyAnswers.ToList();

            int ecardcount = _surveyanswers.Count();
            int perpage = 5;
            int maxpage = (int)Math.Ceiling((decimal)ecardcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<SurveyAnswer> surveyanswers = await db.SurveyAnswers.Skip(start).Take(perpage).ToListAsync();

            return View(surveyanswers);
        }
        public ActionResult New()
        {
            SurveyAnswerEdit surveyans = new SurveyAnswerEdit();
            surveyans.SurveyQuestions = db.SurveyQuestions.ToList();
            return View(surveyans);
        }

        [HttpPost]
        public ActionResult Create(string Answer_New)
        {
            string query = "insert into surveyanswer (Answer)" +
                " values (@ans)";
            SqlParameter[] myparams = new SqlParameter[2];
            myparams[0] = new SqlParameter("@ans", Answer_New);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }

        public ActionResult Delete(int id)
        {
            string query = "delete from surveyanswers where surveyanswerID = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }
    }
}