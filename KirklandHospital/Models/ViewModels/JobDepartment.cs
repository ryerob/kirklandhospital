﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KirklandHospital.Models.ViewModels
{
    public class JobDepartment
    {
        public JobDepartment()
        {

        }

       

        public virtual Job Job { get; set; }

        public IEnumerable<Department> Departments { get; set; }
    }
}