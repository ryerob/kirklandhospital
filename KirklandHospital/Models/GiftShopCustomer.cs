﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KirklandHospital.Models
{
    public class GiftShopCustomer
    {
        [Key]
        public int CustomerID { get; set; }

        [Required, StringLength(255), Display(Name = "First Name")]
        public string CustomerFirstName { get; set; }

        [Required, StringLength(255), Display(Name = "Last Name")]
        public string CustomerLastName { get; set; }

        [Required, StringLength(255), Display(Name = "Email")]
        public string CustomerEmail { get; set; }

        [Required, StringLength(255), Display(Name = "Payment Method")]
        public string CustomerPaymentMethod { get; set; }

        [ForeignKey("ProductID")]
        public int ProductID { get; set; }

       
        public DateTime PurchaseDate { get; set; }

        public virtual GiftShopProduct GiftShopProduct { get; set; }  //a customer can buy just one item at the time, there is no a shopping cart
   
    }
}
