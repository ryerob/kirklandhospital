﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KirklandHospital.Models
{
    public class Page
    {
        [Key]
        public int PageID { get; set; }

        [Required, StringLength(255), Display(Name = "Page Title")]
        public string PageTitle { get; set; }

        [Required, StringLength(int.MaxValue), Display(Name = "Page Content")]
        public string PageContent { get; set; }
    }
}
