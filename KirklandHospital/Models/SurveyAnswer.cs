﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KirklandHospital.Models
{
    public class SurveyAnswer
    {
        [Key]
        public int SurveyAnswerID { get; set; }

        [ForeignKey("SurveyQuenstion")]
        public int SurveyQuestionID { get; set; }

        public virtual SurveyQuestion SurveyQuestion { get; set; }

        [ForeignKey("Survey")]
        public int SurveyID { get; set; }

        public virtual Survey Survey { get; set; }

        [Required, StringLength(255), Display(Name = "Answer")]
        public string Answer { get; set; }
    }
}
