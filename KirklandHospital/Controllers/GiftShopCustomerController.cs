﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using KirklandHospital.Models;
using KirklandHospital.Data;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using KirklandHospital.Models.ViewModels;
using System;


namespace KirklandHospital.Controllers
{
    public class GiftShopCustomerController : Controller
    {
        private readonly KirklandDBContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);
        public GiftShopCustomerController(KirklandDBContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public async Task<ActionResult> List(int pagenumber)
        {
            var _customers = await db.GiftShopCustomers.Include(p => p.GiftShopProduct).ToListAsync();
            int customercount = _customers.Count();
            int perpage = 7;
            int maxpage = (int)Math.Ceiling((decimal)customercount / perpage) - 1;
            if (maxpage< 0) maxpage = 0;
            if (pagenumber< 0) pagenumber = 0;
            if (pagenumber > maxpage) pagenumber = maxpage;
            int start = perpage * pagenumber;
            ViewData["pagenumber"] = (int) pagenumber;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenumber + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
             }

            List<GiftShopCustomer> GiftShopCustomers = await db.GiftShopCustomers.Include(p => p.GiftShopProduct).Skip(start).Take(perpage).ToListAsync();
            //prevent regular visitors from access the list of customers
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }

            return View(GiftShopCustomers);
        }

        public ActionResult Create()
        {
            GiftShopCustomerEdit giftshopView = new GiftShopCustomerEdit();
            giftshopView.GiftShopProducts = db.GiftShopProducts.ToList();

            return View(giftshopView);
        }
        [HttpPost]
        
        public ActionResult Create(string CustomerFirstName_New, string CustomerLastName_New, string CustomerEmail_New, string CustomerPaymentMethod_New, int CustomerProduct_New)
        {
            DateTime PurchaseDate_New = DateTime.Now;
            
           
             
            string qst = "INSERT into GiftShopCustomers (CustomerFirstName, CustomerLastName, CustomerEmail, CustomerPaymentMethod, ProductID, PurchaseDate) " +
                "values (@fname, @lname, @email, @paymentmethod, @product, @date)";


            SqlParameter[] parameters = {
            new SqlParameter("@fname", CustomerFirstName_New),
            new SqlParameter("@lname", CustomerLastName_New),
            new SqlParameter("@email", CustomerEmail_New),
            new SqlParameter("@paymentmethod", CustomerPaymentMethod_New),
            new SqlParameter("@product", CustomerProduct_New),
            new SqlParameter("@date", PurchaseDate_New)
            };

          
            db.Database.ExecuteSqlCommand(qst, parameters);

            // return RedirectToAction("Index", "Home");
            return RedirectToAction("SuccessMsg");
        }
        public ActionResult SuccessMsg(int id)
        {
            return View();
        }

        //edit throws an exception on save changes, need to fix---FIXED!
        public async Task<ActionResult> Edit(int? id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }

            GiftShopCustomerEdit customerEdit = new GiftShopCustomerEdit();
            customerEdit.GiftShopCustomer = db.GiftShopCustomers.Include(p => p.GiftShopProduct).SingleOrDefault(c => c.CustomerID == id);
            customerEdit.GiftShopProducts = db.GiftShopProducts.ToList();
            return View(customerEdit);
        }
        [HttpPost]
        public ActionResult Edit(int? id, string CustomerFirstName, string CustomerLastName, string CustomerEmail, string CustomerPaymentMethod, 
            int CustomerProduct)
        {
            if ((id == null) || (db.GiftShopCustomers.Find(id) == null))
            {
                return NotFound();
            }

            string qst = "UPDATE GiftShopCustomers SET CustomerFirstName = @fname, CustomerLastName = @lname, CustomerEmail = @email, CustomerPaymentMethod = @method, ProductID = @product" +
                " WHERE CustomerID = @id ";
            SqlParameter[] parameters = {
                new SqlParameter("@fname", CustomerFirstName),
                new SqlParameter("@lname", CustomerLastName),
                new SqlParameter("@email", CustomerEmail),
                new SqlParameter("@method", CustomerPaymentMethod),
                new SqlParameter("@product", CustomerProduct),
                new SqlParameter("@id", id)
            };

            db.Database.ExecuteSqlCommand(qst, parameters);
            return RedirectToAction("List");


        }
        public async Task<ActionResult> Delete(int id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }

            string qst = "DELETE FROM GiftShopCustomers WHERE CustomerID = @id";
            db.Database.ExecuteSqlCommand(qst, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}