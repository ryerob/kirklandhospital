﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KirklandHospital.Models.ViewModels
{
    public class FeedbackEdit
    {
        public FeedbackEdit()
        {

        }

        public virtual Feedback Feedback { get; set; }

        public IEnumerable<Department> Departments { get; set; }
    }
}
