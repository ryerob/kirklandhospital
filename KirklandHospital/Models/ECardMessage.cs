﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KirklandHospital.Models
{
    public class ECardMessage
    {
        [Key]
        public int ECardMessageID { get; set; }

        [Required, StringLength(250), Display(Name = "Message")]
        public string ECardMessageText { get; set; }

        [InverseProperty("ECardMessage")]
        public List<ECard> ECards { get; set; }

    }
}
