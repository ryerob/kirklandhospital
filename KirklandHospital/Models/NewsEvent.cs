﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KirklandHospital.Models
{
    public class NewsEvent
    {
        [Key]
        public int NewsEventID { get; set; }

        [Required, StringLength(255), Display(Name = "Title")]
        public string NewsEventTitle { get; set; }

        [Required, StringLength(255), Display(Name = "Description")]
        public string NewsEventDescription { get; set; }

        [Required]
        public string ImagePath { get; set; }
    }
}
