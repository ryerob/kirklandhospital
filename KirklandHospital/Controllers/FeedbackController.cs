﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using KirklandHospital.Models;
using KirklandHospital.Data;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using KirklandHospital.Models.ViewModels;
using System;
using System.Diagnostics;

namespace KirklandHospital.Controllers
{
    public class FeedbackController : Controller
    {
        private readonly KirklandDBContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);
        public FeedbackController(KirklandDBContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        // GET: Feedback
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        public async Task<ActionResult> List(int pagenumber)
          
        {    
            //pagination 

            var _feedbacks = await db.Feedbacks.Include(f => f.Department).ToListAsync();
            int feedbackcount = _feedbacks.Count();
            int perpage = 7;
            int maxpage = (int)Math.Ceiling((decimal)feedbackcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenumber < 0) pagenumber = 0;
            if (pagenumber > maxpage) pagenumber = maxpage;
            int start = perpage * pagenumber;
            ViewData["pagenumber"] = (int)pagenumber;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenumber + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
  
            List<Feedback> Feedbacks = await db.Feedbacks.Include(f => f.Department).Skip(start).Take(perpage).ToListAsync();

            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user != null)
            {
                if (user.AdminID == null) {
                    ViewData["IsAdmin"] = "False";
                    return Forbid();
                }
                else {
                    ViewData["IsAdmin"] = user.AdminID.ToString();
                }
               
            }
            return View(Feedbacks);

        }

        // GET: Feedback/Create
        public ActionResult Create()
        {
            //list of departmrnts to populate the dropdownlist

            //Model defined in Models/ViewModels/FeedbackEdit.cs
            FeedbackEdit fbeditview = new FeedbackEdit();

            //[fbeditview.Departments]=> Set the feedback field of the FeeedbackEdit Object
            //[db.Departments.ToList()]=> Get the list of all departments from DB
            fbeditview.Departments = db.Departments.ToList();

            //GOTO Views/Feedback/Create.cshtml
            return View(fbeditview);
        }

        // POST: Feedback/Create
        [HttpPost]
        
        public ActionResult Create(string UserName_New, string UserEmail_New, int FeedbackDepartment_New, string FeedbackType_New, string FeedbackSubject_New, string FeedbackContent_New)
        {
            DateTime FeedbackPosted_New = DateTime.Now;

            string qst = "INSERT into Feedbacks (DepartmentID, FeedbackUserName, FeedbackUserEmail, FeedbackMessageType, FeedbackMessageSubject, FeedbackMessageContent, FeedbackPosted) " +
                "values (@department, @name, @email, @type, @subject, @content, @date)";

            SqlParameter[] parameters = {
            
            //@department (id) FOREIGN KEY paramter
            new SqlParameter("@department", FeedbackDepartment_New),
             //@name parameter
            new SqlParameter("@name", UserName_New),
              //@email parameter
            new SqlParameter("@email", UserEmail_New),
             //@type parameter
            new SqlParameter("@type", FeedbackType_New),
            //@subject parameter
            new SqlParameter("@subject", FeedbackSubject_New),
            //@content parameter
            new SqlParameter("@content", FeedbackContent_New),
            //@date parameter
             new SqlParameter("@date", FeedbackPosted_New)
            };

         
            db.Database.ExecuteSqlCommand(qst, parameters);
            db.SaveChanges();

            //GOTO: 
            //return RedirectToAction("Index", "Home");
            return RedirectToAction("SuccessMsg");
        }
        public ActionResult SuccessMsg(int id)
        {
            return View();
        }

        public async Task<ActionResult> Edit(int? id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }

            FeedbackEdit feedbackEdit = new FeedbackEdit();
            feedbackEdit.Feedback = db.Feedbacks.Include(f => f.Department).SingleOrDefault(f => f.FeedbackID == id);
            feedbackEdit.Departments = db.Departments.ToList();
            return View(feedbackEdit);
        }

       
        [HttpPost]
        public ActionResult Edit(int? id, int FeedbackDepartment,  string UserName, string UserEmail,  
            string FeedbackType, string FeedbackSubject, string FeedbackContent)
        {
            if ((id == null) || (db.Feedbacks.Find(id) == null))
            {
                return NotFound();
            }
            
            string qst = "UPDATE Feedbacks SET DepartmentID = @department, FeedbackUserName = @name, FeedbackUserEmail = @email, " +
                "FeedbackMessageType = @type, FeedbackMessageSubject = @subject, FeedbackMessageContent = @content WHERE FeedbackID = @id ";
            SqlParameter[] parameters = {
                new SqlParameter("@department", FeedbackDepartment),
                new SqlParameter("@name", UserName),
                new SqlParameter("@email", UserEmail),
                new SqlParameter("@type", FeedbackType),
                new SqlParameter("@subject", FeedbackSubject),
                new SqlParameter("@content", FeedbackContent),
                new SqlParameter("@id", id)
            };

            db.Database.ExecuteSqlCommand(qst, parameters);
            return RedirectToAction("List");


        }

        // Get details of a Feedback that needs to be deleted
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var feedback = await db.Feedbacks.FirstOrDefaultAsync(f => f.FeedbackID == id);

            if (feedback == null)
            {
                return NotFound();
            }
            return View(feedback);
        }


        //Delete 
        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }

            string qst = "DELETE FROM Feedbacks WHERE FeedbackID = @id";
            db.Database.ExecuteSqlCommand(qst, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }
       
       
    }
}