﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using KirklandHospital.Models;
using KirklandHospital.Models.ViewModels;
using KirklandHospital.Data;
using System.Diagnostics;

namespace KirklandHospital.Controllers
{
    public class ECardMessageController : Controller
    {
        private readonly KirklandDBContext db;


        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public ECardMessageController(KirklandDBContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> List()
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }
            return View(db.ECardMessages.ToList());
        }

        public async Task<ActionResult> Create()
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create(string Message_new, ECardMessage ECardMessage)
        {

            string query = "insert into ecardmessages (ECardMessageText) values (@message)";
            SqlParameter[] myparams =
            {
                new SqlParameter("@message", Message_new)
            };
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");

        }


        public async Task<ActionResult> Delete(int id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }
            db.ECardMessages.Remove(db.ECardMessages.Find(id));
            db.SaveChanges();

            return RedirectToAction("List");
        }
    }
}