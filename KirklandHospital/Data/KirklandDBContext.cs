﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Security.Claims;
using System.Threading.Tasks;

using KirklandHospital.Models;

namespace KirklandHospital.Data
{
    public class KirklandDBContext : IdentityDbContext<ApplicationUser>
    {
        public KirklandDBContext(DbContextOptions<KirklandDBContext> options)
            : base(options)
        {

        }

        public DbSet<Job> Jobs { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Donation> Donations { get; set; }
        public DbSet<ECard> ECards { get; set; }
        public DbSet<ECardTemplate> ECardTemplates { get; set; }
        public DbSet<ECardMessage> ECardMessages { get; set; }
        public DbSet<Admin> Admins { get; set; }

        public DbSet<EmergencyAlert> EmergencyAlerts { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<GiftShopCustomer> GiftShopCustomers { get; set; }
        public DbSet<GiftShopProduct> GiftShopProducts { get; set; }

        public DbSet<Page> Page { get; set; }
        public DbSet<FAQ> FAQ { get; set; }
        public DbSet<BabyGallery> BabyGallery { get; set; }

        public DbSet<BookAnAppointment> BookAnAppointments { get; set; }
        public DbSet<NewsEvent> NewsEvents { get; set; }
        public DbSet<SurveyQuestion> SurveyQuestions { get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<SurveyAnswer> SurveyAnswers { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Admin>()
                .HasOne(a => a.User)
                .WithOne(u => u.Admin)
                .HasForeignKey<ApplicationUser>(u => u.AdminID);

            modelBuilder.Entity<ECard>()
                .HasOne(ECard => ECard.ECardMessage)
                .WithMany(ecardmsg=>ecardmsg.ECards)
                .HasForeignKey(ECard => ECard.ECardMessageID)
                .OnDelete(DeleteBehavior.Cascade); 

            modelBuilder.Entity<ECard>()
                .HasOne(ECard => ECard.ECardTemplate)
                .WithMany(ecardtemp=>ecardtemp.ECards)
                .HasForeignKey(ECard => ECard.ECardTemplateID)
                .OnDelete(DeleteBehavior.Cascade); 

            modelBuilder.Entity<Feedback>()
               .HasOne(Feedback => Feedback.Department)
               .WithMany(d => d.Feedbacks)
               .HasForeignKey(Feedback => Feedback.DepartmentID);

            modelBuilder.Entity<GiftShopCustomer>()
              .HasOne(GiftShopCustomer => GiftShopCustomer.GiftShopProduct)
              .WithMany(p => p.GiftShopCustomers)
              .HasForeignKey(GiftShopCustomer => GiftShopCustomer.ProductID);

           

            modelBuilder.Entity<Job>()
                .HasOne(Job => Job.Department)
                .WithMany(d => d.Jobs)
                .HasForeignKey(Job => Job.DepartmentID);

            modelBuilder.Entity<SurveyAnswer>()
                 .HasKey(s => new { s.SurveyID, s.SurveyQuestionID });

            modelBuilder.Entity<SurveyAnswer>()
               .HasOne(s => s.Survey)
               .WithMany(d => d.SurveyAnswers)
               .HasForeignKey(s => s.SurveyID);

            modelBuilder.Entity<SurveyAnswer>()
               .HasOne(s => s.SurveyQuestion)
               .WithMany(d => d.SurveyAnswers)
               .HasForeignKey(s => s.SurveyQuestionID);




            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Job>().ToTable("Jobs");
            modelBuilder.Entity<Department>().ToTable("Departments");
            modelBuilder.Entity<Donation>().ToTable("Donations");
            modelBuilder.Entity<ECard>().ToTable("ECards");
            modelBuilder.Entity<ECardTemplate>().ToTable("ECardTemplates");
            modelBuilder.Entity<ECardMessage>().ToTable("ECardMessages");
            modelBuilder.Entity<Admin>().ToTable("Admins");

            modelBuilder.Entity<EmergencyAlert>().ToTable("EmergencyAlerts");
            modelBuilder.Entity<Feedback>().ToTable("Feedbacks");
            modelBuilder.Entity<GiftShopCustomer>().ToTable("GiftShopCustomers");
            modelBuilder.Entity<GiftShopProduct>().ToTable("GiftShopProducts");

            modelBuilder.Entity<Page>().ToTable("Pages");
            modelBuilder.Entity<FAQ>().ToTable("FAQ");
            modelBuilder.Entity<BabyGallery>().ToTable("BabyGallery");

            modelBuilder.Entity<BookAnAppointment>().ToTable("BookAnAppointments");
            modelBuilder.Entity<Survey>().ToTable("Surveys");
            modelBuilder.Entity<SurveyQuestion>().ToTable("SurveyQuestions");
            modelBuilder.Entity<SurveyAnswer>().ToTable("SurveyAnswers");
            modelBuilder.Entity<NewsEvent>().ToTable("NewsEvents");

        }

    }
}
