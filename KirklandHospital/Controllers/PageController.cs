﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using KirklandHospital.Models;
using KirklandHospital.Data;
using Microsoft.AspNetCore.Http;


namespace KirklandHospital.Controllers
{
    public class PageController : Controller
    {
        private readonly KirklandDBContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public PageController(KirklandDBContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }
        // redirect to list view if on index
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        // get request to list pages
        public async Task<ActionResult> List(int pagenumber)
        {
            // check if user logged in and admin
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }
            // pagination logic
            var _pages = await db.Page.ToListAsync();
            int pagecount = _pages.Count();
            int perpage = 10;
            int maxpage = (int)Math.Ceiling((decimal)pagecount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenumber < 0) pagenumber = 0;
            if (pagenumber > maxpage) pagenumber = maxpage;
            int start = perpage * pagenumber;
            ViewData["pagenumber"] = (int)pagenumber;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenumber + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            List<Page> pages = await db.Page.Skip(start).Take(perpage).ToListAsync();

            return View(pages);
        }
        // get request to display specified details
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var page = await db.Page.FirstOrDefaultAsync(p => p.PageID == id);
            
            if (page == null)
            {
                return NotFound();
            }

            return View(page);
        }
        // create view if logged in as admin
        public async Task<ActionResult> CreateAsync()
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }
            return View();
        }
        // create new page post values
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("PageID,PageTitle,PageContent")] Page page)
        {
            if (ModelState.IsValid)
            {
                db.Page.Add(page);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        // get and display edit page values
        public async Task<IActionResult> Edit(int? id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }

            if (id == null)
            {
                return NotFound();
            }

            var page = await db.Page.FindAsync(id);
            if (page == null)
            {
                return NotFound();
            }
            return View(page);
        }
        // post request to insert new updated data
        [HttpPost]
        public ActionResult Edit(int? id, string PageTitle, string PageContent)
        {
            // check id to make sure page exists
            if ((id == null) || (db.Page.Find(id) == null))
            {
                return NotFound();
            }
            // define query and parameters for database interaction
            string query = "update pages set PageTitle=@title, PageContent=@content where pageid=@id ";
            SqlParameter[] myparams = {
                new SqlParameter("@title", PageTitle),
                new SqlParameter("@content", PageContent),
                new SqlParameter("@id", id)
            };

            db.Database.ExecuteSqlCommand(query, myparams);
            return RedirectToAction("List");

        }
        // get request to retrieve info for delete if admin logged in
        public async Task<IActionResult> Delete(int? id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }
            // check if page id exists and then display data
            if (id == null)
            {
                return NotFound();
            }
            var page = await db.Page.FirstOrDefaultAsync(m => m.PageID == id);
            if (page == null)
            {
                return NotFound();
            }
            return View(page);
        }
        // post request to delete information
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var page = await db.Page.FindAsync(id);
            db.Page.Remove(page);
            await db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}