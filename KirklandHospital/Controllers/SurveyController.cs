﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using KirklandHospital.Data;
using KirklandHospital.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KirklandHospital.Controllers
{
    public class SurveyController : Controller
    {
        private readonly KirklandDBContext db;
        public IActionResult Index()
        {
            return View();
        }
        public SurveyController(KirklandDBContext context)
        {
            db = context;
        }
        public async Task<ActionResult> List(int pagenum)
        {
            List<Survey> _surveys = db.Surveys.ToList();

            int ecardcount = _surveys.Count();
            int perpage = 5;
            int maxpage = (int)Math.Ceiling((decimal)ecardcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<Survey> surveys = await db.Surveys.Skip(start).Take(perpage).ToListAsync();

            return View(surveys);
        }

        public ActionResult New()
        {

            return View();
        }
        public ActionResult Delete(int id)
        {
            string query = "delete from surveys where surveyID = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }

    }
}