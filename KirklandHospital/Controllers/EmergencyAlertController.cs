﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using KirklandHospital.Models;
using KirklandHospital.Data;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using KirklandHospital.Models.ViewModels;
using System;
using System.Diagnostics;

namespace KirklandHospital.Controllers
{
    public class EmergencyAlertController : Controller
    {
        private readonly KirklandDBContext db;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public EmergencyAlertController(KirklandDBContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }


        // GET: EMessage List for Users to read only need to add 0 as a default value and  add if  statement in List view if(isPublished = "1"){}
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
       //
        public async Task<ActionResult> List(int pagenumber)
        {
           
            var _feedbacks = await db.Feedbacks.Include(f => f.Department).ToListAsync();
            int feedbackcount = _feedbacks.Count();
            int perpage = 7;
            int maxpage = (int)Math.Ceiling((decimal)feedbackcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenumber < 0) pagenumber = 0;
            if (pagenumber > maxpage) pagenumber = maxpage;
            int start = perpage * pagenumber;
            ViewData["pagenumber"] = (int)pagenumber;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenumber + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<EmergencyAlert> emergencyAlerts = await db.EmergencyAlerts.Skip(start).Take(perpage).ToListAsync();

            //query to fetch the published data (IsPublished = 1) to display on user view
            string qst = "SELECT * FROM EmergencyAlerts WHERE IsPublished=1";
            IEnumerable<EmergencyAlert> EmergencyAlerts = db.EmergencyAlerts.FromSql(qst);

            //query to fetch all data from EmergencyAlerts table to display in the table on admin view
            string queryAdmin = "SELECT * FROM EmergencyAlerts";
            IEnumerable<EmergencyAlert> EmergencyAlertsAdmin = db.EmergencyAlerts.FromSql(queryAdmin);

            var user = await GetCurrentUserAsync();
            //if user exists and they are not admin then they don't have acces to admin part in list view
            //if user exists and they're admin they have access to admin part in list view

            if (user != null)
            {
                if (user.AdminID == null)
                {
                    ViewData["IsAdmin"] = "False";
                    return Forbid();
                }
                else {
                    ViewData["IsAdmin"] = user.AdminID.ToString();
                }
             
                return View(EmergencyAlertsAdmin);
            }
            else { 

                ViewData["IsAdmin"] = "None";
                return View(EmergencyAlerts);
            }

        }

        //only logged-in admin is able to create new alert messages
        public async Task<ActionResult> Create(int id)
        {
            var user = await GetCurrentUserAsync();

            if (user == null)
            {
                return BadRequest();
            }

            if (user.AdminID == null)
            {
                return Forbid();
            }

            return View();
        }

       
        // POST: EMessege/Create
        //MAKE IsPublished a checkbox and if it's checked value=1(published) if not by default it's=0(not published) !!!!!!!!!
        [HttpPost]
        public ActionResult Create(string EmergencyMessageTitle_New, string EmergencyMessageContent_New, bool IsPublished_New)
        {
            int isPublished;
            DateTime EmergencyaDate_New = DateTime.Now;
           // Debug.WriteLine(IsPublished_New);
            if (IsPublished_New)
            {
                isPublished = 1;

            }
            else
            {
                isPublished = 0;
            }
            //Query statement to insert data in the table
            string qst = "INSERT into EmergencyAlerts (EmergencyMessageTitle, EmergencyMessageContent, IsPublished, EmergencyMessageCreated)" +
                " values (@title, @content, @ispublished, @date)";

     
            SqlParameter[] parameters = {
            
            //@title parameter
            new SqlParameter("@title", EmergencyMessageTitle_New),
            //@content parameter
            new SqlParameter("@content", EmergencyMessageContent_New),  
            //@ispublished parameter
            new SqlParameter("@ispublished", isPublished),
            //@date parameter
            new SqlParameter("@date", EmergencyaDate_New)
            };

            db.Database.ExecuteSqlCommand(qst, parameters);

            //Go to: 
            return RedirectToAction("List");
        }



        //only logged-in admin is able to edit alert messages
        public async Task<ActionResult> Edit(int id)
        {
            var user = await GetCurrentUserAsync();

            if (user == null)
            {
                return BadRequest();
            }

            if (user.AdminID == null)
            {
                return Forbid();
            }

            return View(db.EmergencyAlerts.Find(id));
        }

         
        [HttpPost]
        public ActionResult Edit(int? id, string EmergencyMessageTitle, string EmergencyMessageContent, bool IsPublished)
        {
            int isPublished;
            DateTime EmergencyaDate_New = DateTime.Now;
           // Debug.WriteLine(IsPublished);
            if (IsPublished)
            {
                isPublished = 1;

            }
            else
            {
                isPublished = 0;
            }
            if ((id == null) || (db.EmergencyAlerts.Find(id) == null))
            {
                return NotFound();
            }
           //Query to update the data in the table whith the specific id
            string qst = "UPDATE EmergencyAlerts SET EmergencyMessageTitle = @title, EmergencyMessageContent = @content, IsPublished = @ispublished  WHERE EmergencyID = @id ";
            SqlParameter[] parameters = {
                new SqlParameter("@title", EmergencyMessageTitle),
                new SqlParameter("@content", EmergencyMessageContent),
                new SqlParameter("@ispublished", isPublished),
                new SqlParameter("@id", id)
            };

            db.Database.ExecuteSqlCommand(qst, parameters);
            return RedirectToAction("List");


        }
        //only logged-in admin is able to delete alert messages
        public async Task<ActionResult> Delete(int id)
        {
            var user = await GetCurrentUserAsync();

            if (user == null)
            {
                return BadRequest();
            }

            if (user.AdminID == null)
            {
                return Forbid();
            }
            
            //Query to delete message by setting IsPublished value to = 0 that means the mesaage isn't published 
            //however, the actual message is not deleted from the table 
            string qst = "UPDATE EmergencyAlerts SET IsPublished = 0   WHERE EmergencyId = @id";
            db.Database.ExecuteSqlCommand(qst, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}