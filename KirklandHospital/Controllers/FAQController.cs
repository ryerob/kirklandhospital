﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using KirklandHospital.Models;
using KirklandHospital.Data;
using Microsoft.AspNetCore.Http;


namespace KirklandHospital.Controllers
{
    public class FAQController : Controller
    {
        private readonly KirklandDBContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public FAQController(KirklandDBContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public async Task<ActionResult> List(int pagenumber)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }

            var _faqs = await db.FAQ.ToListAsync();
            int faqcount = _faqs.Count();
            int perpage = 5;
            int maxpage = (int)Math.Ceiling((decimal)faqcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenumber < 0) pagenumber = 0;
            if (pagenumber > maxpage) pagenumber = maxpage;
            int start = perpage * pagenumber;
            ViewData["pagenumber"] = (int)pagenumber;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenumber + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            List<FAQ> faqs = await db.FAQ.Skip(start).Take(perpage).ToListAsync();

            return View(faqs);
        }

        // public list of FAQs
        public async Task<ActionResult> PublicList(int pagenumber)
        {
            var _faqs = await db.FAQ.ToListAsync();

            int faqcount = _faqs.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)faqcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenumber < 0) pagenumber = 0;
            if (pagenumber > maxpage) pagenumber = maxpage;
            int start = perpage * pagenumber;
            ViewData["pagenumber"] = (int)pagenumber;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenumber + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            List<FAQ> faqs = await db.FAQ.Skip(start).Take(perpage).ToListAsync();

            return View(_faqs);
        }

        // display details of a faq
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var faq = await db.FAQ.FirstOrDefaultAsync(p => p.FaqID == id);

            if (faq == null)
            {
                return NotFound();
            }
            return View(faq);
        }

        public async Task<IActionResult> PDetails(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var faq = await db.FAQ.FirstOrDefaultAsync(p => p.FaqID == id);

            if (faq == null)
            {
                return NotFound();
            }
            return View(faq);
        }

        public async Task<ActionResult> CreateAsync()
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("FaqID, Question, Answer, Date")] FAQ fAQ)
        { 

            if (ModelState.IsValid)
            {
                db.FAQ.Add(fAQ);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        // get and display edit page values
        public async Task<IActionResult> Edit(int? id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }

            if (id == null)
            {
                return NotFound();
            }

            var faq = await db.FAQ.FindAsync(id);
            if (faq == null)
            {
                return NotFound();
            }
            return View(faq);
        }

        [HttpPost]
        public ActionResult Edit(int? id, string Question, string Answer, DateTime Date)
        {
            if ((id == null) || (db.FAQ.Find(id) == null))
            {
                return NotFound();
            }

            string query = "update faq set Question=@question, Answer=@answer, Date=@date where faqid=@id ";
            SqlParameter[] myparams = {
                new SqlParameter("@question", Question),
                new SqlParameter("@answer", Answer),
                new SqlParameter("@date", Date),
                new SqlParameter("@id", id)
            };

            db.Database.ExecuteSqlCommand(query, myparams);
            return RedirectToAction("List");

        }
        // get request to retrieve inforamtion
        public async Task<IActionResult> Delete(int? id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }
            if (id == null)
            {
                return NotFound();
            }
            var faq = await db.FAQ.FirstOrDefaultAsync(m => m.FaqID == id);
            if (faq == null)
            {
                return NotFound();
            }
            return View(faq);
        }

        // post request to delete information
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var page = await db.FAQ.FindAsync(id);
            db.FAQ.Remove(page);
            await db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

    }    
}