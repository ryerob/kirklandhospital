﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using KirklandHospital.Models;
using KirklandHospital.Models.ViewModels;
using KirklandHospital.Data;
using System.Diagnostics;
using SendGrid;
using SendGrid.Helpers.Mail;



namespace KirklandHospital.Controllers
{
    public class ECardController : Controller
    {
        private readonly KirklandDBContext db;

        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public ECardController(KirklandDBContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        public IActionResult Index()
        {
            return View();
        }


        //Trying to get email to send via sendgrid but can't get it to work
        static async Task Execute()
        {
            var apiKey = Environment.GetEnvironmentVariable("SENDGRID_APIKEY");
            var client = new SendGridClient(apiKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress("jenna.b.greenberg@gmail.com", "KDH Team"),
                Subject = "Hello World from the SendGrid CSharp SDK!",
                PlainTextContent = "Hello, Email!",
                HtmlContent = "<strong>Hello, Email!</strong>"
            };
            msg.AddTo(new EmailAddress("jenna.b.greenberg@gmail.com", "Test User"));
            var response = await client.SendEmailAsync(msg);
        }

        public async Task<ActionResult> List(int pagenum)
        {
                var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
                {
                    return Forbid();
                }

                var _ecards = await db.ECards.ToListAsync();
            int ecardcount = _ecards.Count();
            int perpage = 5;
            int maxpage = (int)Math.Ceiling((decimal)ecardcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<ECard> ecards = await db.ECards.Skip(start).Take(perpage).ToListAsync();
            return View(ecards);
        }

        public ActionResult Create()
        {

            ECardCreate ecardcreateview = new ECardCreate();

            ecardcreateview.ECardTemplates = db.ECardTemplates.ToList();
            ecardcreateview.ECardMessages = db.ECardMessages.ToList();

            return View(ecardcreateview);
        }


       
        [HttpPost]
        public ActionResult Create(int ECardID, int ECardMessage_new, int ECardTemplate_new, string CustomMessage_new, 
            string UserEmail_new, string PatientEmail_new, DateTime Date_new, ECard ecard)
        {
            var current_date = DateTime.Now;
            ecard.ECardDate = current_date;

            if (CustomMessage_new == null) {
                CustomMessage_new = " ";
            }
           

            string query = "insert into ecards (ECardMessageID, ECardTemplateID, ECardCustomMessage, ECardUserEmail," +
                "ECardPatientEmail, ECardDate)" +
                "values (@message, @template, @custommessage, @useremail, @patientemail, @date)";
            SqlParameter[] myparams =
            {
                new SqlParameter("@message", ECardMessage_new),
                new SqlParameter("@template", ECardTemplate_new),
                new SqlParameter("@custommessage", CustomMessage_new),
                new SqlParameter("@useremail", UserEmail_new),
                new SqlParameter("@patientemail", PatientEmail_new),
                new SqlParameter("@date", ecard.ECardDate)
            };
            db.Database.ExecuteSqlCommand(query, myparams);

            //find the highest id in ecards to return the one that was just created for the show details page.
            //i know this is wrong but I can't find the sql_insert_id effective equivalent in ef core
            int newID = db.ECards.Max(e => e.ECardID);
            //Execute().Wait();

           // var eid = ecard.ECardID;


            return RedirectToAction("Show/"+ newID);
            

        }
        public async Task<ActionResult> Delete(int id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }

            db.ECards.Remove(db.ECards.Find(id));
            db.SaveChanges();

            return RedirectToAction("List");
        }

        public ActionResult Show(int id)
        {
            
            ECard ecardview = db.ECards.Include(e => e.ECardMessage).Include(e => e.ECardTemplate).SingleOrDefault(e => e.ECardID == id);


            return View(ecardview);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }

            ECardCreate ecardcreateview = new ECardCreate();

            ecardcreateview.ECardTemplates = db.ECardTemplates.ToList();
            ecardcreateview.ECardMessages = db.ECardMessages.ToList();
            ecardcreateview.ECard = db.ECards.Find(id);

            return View(ecardcreateview);

        }

        //Edit not working, can't seem to get the set values from the ECard ID it's pulling data from.
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(int id, int ECardMessage, int ECardTemplate, string CustomMessage,
            string UserEmail, string PatientEmail, ECard ecard)
        {

            string query = "update ecards set ECardMessageID = @message, ECardTemplateID = @template, ECardCustomMessage = @custommessage," +
                " ECardUserEmail = @useremail, ECardPatientEmail = @patientemail where ecardid = @id";


            SqlParameter[] myparams =
            {
                new SqlParameter ("@id", id),
                new SqlParameter("@message", ECardMessage),
                new SqlParameter("@template", ECardTemplate),
                new SqlParameter("@custommessage", CustomMessage),
                new SqlParameter("@useremail", UserEmail),
                new SqlParameter("@patientemail", PatientEmail),
            };
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }
    }
}