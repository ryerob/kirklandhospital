﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using KirklandHospital.Data;
using KirklandHospital.Models;
using KirklandHospital.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KirklandHospital.Controllers
{
    public class SurveyQuestionController : Controller
    {
        private readonly KirklandDBContext db;
        public IActionResult Index()
        {
            return View();
        }
        public SurveyQuestionController(KirklandDBContext context)
        {
            db = context;
        }
        public async Task<ActionResult> List(int pagenum)
        {
            List<SurveyQuestion> _surveyquestions = db.SurveyQuestions.ToList();

            int ecardcount = _surveyquestions.Count();
            int perpage = 5;
            int maxpage = (int)Math.Ceiling((decimal)ecardcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<SurveyQuestion> surveyquestions = await db.SurveyQuestions.Skip(start).Take(perpage).ToListAsync();

            return View(surveyquestions);
        }

        public ActionResult New()
        {
            SurveyQuestionEdit s = new SurveyQuestionEdit();
            return View(s);
        }

        [HttpPost]
        public ActionResult Create(string Question_New)
        {
            string query = "insert into surveyquestions (Question)" +
                " values (@question)";
            SqlParameter[] myparams = new SqlParameter[1];
            myparams[0] = new SqlParameter("@question", Question_New);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }

        public ActionResult Delete(int id)
        {
            string query = "delete from surveyquestions where surveyquestionID = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }

        public ActionResult Show(int? id)
        {
            if((id ==null) || db.SurveyQuestions.Find(id) == null)
            {
                return NotFound();
            }

            SurveyQuestion question = new SurveyQuestion();
            question = db.SurveyQuestions.Find(id);
            return View(question);
        }

        /*public ActionResult Edit(int id)
        {
            NewsEvent newseventedit = new NewsEvent();
            newseventedit = db.NewsEvents.Find(id);

            return View(newseventedit);
        }*/

        [HttpPost]
        public ActionResult Edit(int? id, string Question)
        {
            if ((id == null) || (db.SurveyQuestions.Find(id) == null))
            {
                return NotFound();
            }

            string query = "update surveyquestions set Question = @question where SurveyQuestionID = @id ";
            SqlParameter[] myparams = {
                new SqlParameter("@question", Question),
                new SqlParameter("id", @id)
            };

            db.Database.ExecuteSqlCommand(query, myparams);
            return RedirectToAction("List");

        }
    }
}