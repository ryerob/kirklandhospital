﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KirklandHospital.Models
{
    public class BookAnAppointment
    {
        [Key]
        public int BookAnAppointmentID { get; set; }

        public string userid { get; set; }

        [Required, StringLength(255), Display(Name = "First Name")]
        public string PatientFName { get; set; }

        [Required, StringLength(255), Display(Name = "Last Name")]
        public string PatientLName { get; set; }

        [Required, StringLength(255), Display(Name = "Address")]
        public string PatientAddress { get; set; }

        [Required, StringLength(255), Display(Name = "City")]
        public string PatientCity { get; set; }

        [Required, StringLength(255), Display(Name = "Province")]
        public string PatientProvince { get; set; }

        [Required, StringLength(255), Display(Name = "Postal Code")]
        public string PatientPostalCode { get; set; }

        [Required, StringLength(255), Display(Name = "Phone")]
        public string PatientPhone { get; set; }

        [Required, StringLength(255), Display(Name = "Email")]
        public string PatientEmail { get; set; }

        [Required, StringLength(255), Display(Name = "Gender")]
        public string PatientGender { get; set; }

        [Required, StringLength(255), Display(Name = "DOB")]
        public string PatientDOB { get; set; }

        [Required, StringLength(255), Display(Name = "Appointment Date")]
        public string AppointmentDate { get; set; }

        [Required, StringLength(255), Display(Name = "Appointment Time")]
        public string AppointmentTime { get; set; }

        //need userid

    }
}
