﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KirklandHospital.Models
{
    public class FAQ
    {
        [Key]
        public int FaqID { get; set; }

        [Required, StringLength(int.MaxValue), Display(Name = "Question")]
        public string Question { get; set; }

        [Required, StringLength(int.MaxValue), Display(Name = "Answer")]
        public string Answer { get; set; }

        [DataType(DataType.Date), Display(Name = "Date")]
        public string Date { get; set; }
    }
}
