﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KirklandHospital.Models
{
    public class SurveyQuestion
    {
        [Key]
        public int SurveyQuestionID { get; set; }

        [Required, StringLength(255), Display(Name = "Question")]
        public string Question { get; set; }

        [InverseProperty("SurveyQuestion")]
        public virtual List<SurveyAnswer> SurveyAnswers { get; set; }
    }
}
