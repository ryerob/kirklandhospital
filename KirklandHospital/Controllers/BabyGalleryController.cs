﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using KirklandHospital.Models;
using KirklandHospital.Data;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace KirklandHospital.Controllers
{
    public class BabyGalleryController : Controller
    {

        private readonly KirklandDBContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public BabyGalleryController(KirklandDBContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }
        // redirect to list view if on index
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        // get request to return list of 
        public async Task<ActionResult> PublicList()
        {
            var _galleries = await db.BabyGallery.ToListAsync();
            return View(_galleries);
        }
        // get request to admin list of pages
        public async Task<ActionResult> List(int pagenumber)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }

            var _galleries = await db.BabyGallery.ToListAsync();
            int gallerycount = _galleries.Count();
            int perpage = 10;
            int maxpage = (int)Math.Ceiling((decimal)gallerycount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenumber < 0) pagenumber = 0;
            if (pagenumber > maxpage) pagenumber = maxpage;
            int start = perpage * pagenumber;
            ViewData["pagenumber"] = (int)pagenumber;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenumber + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            List<BabyGallery> babyGalleries = await db.BabyGallery.Skip(start).Take(perpage).ToListAsync();

            return View(babyGalleries);
        }
        // get request to display specified details
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var babygallery = await db.BabyGallery.FirstOrDefaultAsync(p => p.PostID == id);

            if (babygallery == null)
            {
                return NotFound();
            }

            return View(babygallery);
        }
        // create view if logged in as admin
        public async Task<ActionResult> Create()
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }
            return View();
        }
        // create new page post values
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("PostID, PostTitle, PostContent, FamilyName, BirthDate, ImagePath")] BabyGallery babygallery)
        {

            if (ModelState.IsValid)
            {
                db.BabyGallery.Add(babygallery);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }


        }

        // get and display edit page values
        public async Task<IActionResult> Edit(int? id)
        {

            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }

            if (id == null)
            {
                return NotFound();
            }

            var babygallery = await db.BabyGallery.FindAsync(id);
            if (babygallery == null)
            {
                return NotFound();
            }
            return View(babygallery);
        }
        // post request to insert new updated data
        [HttpPost]
        public ActionResult Edit(int? id, string PostTitle, string PostContent, string FamilyName, DateTime BirthDate, string ImagePath)
        {
            if ((id == null) || (db.BabyGallery.Find(id) == null))
            {
                return NotFound();
            }

            string query = "update babygallery set PostTitle=@title, PostContent=@content, FamilyName=@name, BirthDate=@bday, ImagePath=@path where postid=@id ";
            SqlParameter[] myparams = {
                new SqlParameter("@title", PostTitle),
                new SqlParameter("@content", PostContent),
                new SqlParameter("@name", FamilyName),
                new SqlParameter("@bday", BirthDate),
                new SqlParameter("@path", ImagePath),
                new SqlParameter("@id", id)
            };

            db.Database.ExecuteSqlCommand(query, myparams);
            return RedirectToAction("List");
        }

        // get request to retrieve info for delete if admin logged in
        public async Task<IActionResult> Delete(int? id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }
            // check if page id exists and then display data
            if (id == null)
            {
                return NotFound();
            }
            var babygallery = await db.BabyGallery.FirstOrDefaultAsync(m => m.PostID == id);
            if (babygallery == null)
            {
                return NotFound();
            }
            return View(babygallery);
        }
        // post request to delete information
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var babygallery = await db.BabyGallery.FindAsync(id);
            db.BabyGallery.Remove(babygallery);
            await db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}