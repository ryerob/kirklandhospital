﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KirklandHospital.Models
{
    public class EmergencyAlert
    {
        [Key]
        public int EmergencyID { get; set; }

        [Required, StringLength(255), Display(Name = "Emergency Message Title")]
        public string EmergencyMessageTitle { get; set; }

        [Required, StringLength(int.MaxValue), Display(Name = "Emergency Message Content")]
        public string EmergencyMessageContent { get; set; }

        //should auto generate when the message is posted
        public DateTime EmergencyMessageCreated { get; set; }

        //1=>yes 
        //0=> no 
        [Display(Name = "Is Published")]
        public int IsPublished { get; set; }

    }
}
