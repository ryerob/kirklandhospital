﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using KirklandHospital.Models;
using KirklandHospital.Data;
using System.Diagnostics;

namespace KirklandHospital.Controllers
{
    public class DepartmentController : Controller
    {
        private readonly KirklandDBContext db;

        public DepartmentController(KirklandDBContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            return View(db.Departments.ToList());
        }
        
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(string DepartmentName_new)
        {
            string query = "insert into departments (DepartmentName)" +
                "values(@name)";
            SqlParameter[] myparams =
            {
                new SqlParameter("@name",DepartmentName_new)
            };
            db.Database.ExecuteSqlCommand(query, myparams);
            
            return RedirectToAction("List");
        }

    }
}