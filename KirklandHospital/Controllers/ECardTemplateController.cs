﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using KirklandHospital.Models;
using KirklandHospital.Models.ViewModels;
using KirklandHospital.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace KirklandHospital.Controllers
{
    public class ECardTemplateController : Controller
    {
        private readonly KirklandDBContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);


        public ECardTemplateController(KirklandDBContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;

        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> List()
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }
            return View(db.ECardTemplates.ToList());
        }

        public async Task<ActionResult> Create()
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create(string Title_new, IFormFile img, ECardTemplate ECardTemplate)
        {
            //Still need to validate for file types and ensure unique path names by adding current DateTime

            var webRoot = _env.WebRootPath;
            if (img !=null)
            {
                //checking the extension and extracting that from the image
                var valtypes = new[] { "jpeg", "jpg", "png", "gif" };
                var extension = Path.GetExtension(img.FileName).Substring(1);

                if (valtypes.Contains(extension))
                {
                    //giving each image a unique name based on date time and appending the extension
                    string imgtitle = DateTime.Now.ToString("yyyymmddMMss") + "." + extension;
                    string path = Path.Combine(webRoot, "images/ECard");
                    path = Path.Combine(path, imgtitle);

                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        img.CopyTo(stream);
                    }

                    //Saving just the filename in the database, combine with the full path in the view to display
                    ECardTemplate.ECardTemplatePath = Path.GetFileName(path);
                }
                



                string query = "insert into ecardtemplates (ECardTemplateTitle, ECardTemplatePath) values (@title, @img)";
                SqlParameter[] myparams =
                {
                    new SqlParameter("@title", Title_new ),
                    new SqlParameter("@img", ECardTemplate.ECardTemplatePath)
                };
                db.Database.ExecuteSqlCommand(query, myparams);

            }
            return RedirectToAction("List");
        }
        public async Task<ActionResult> Delete(int id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }
            db.ECardTemplates.Remove(db.ECardTemplates.Find(id));
            db.SaveChanges();

            return RedirectToAction("List");
        }
    }
}