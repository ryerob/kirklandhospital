﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KirklandHospital.Models
{
    public class Department
    {
        [Key]
        public int DepartmentID { get; set; }

        [Required, StringLength(200), Display(Name = "Department Name")]
        public string DepartmentName { get; set; }

        [InverseProperty("Department")]
        public virtual List<Feedback> Feedbacks { get; set; }

        //[InverseProperty("Department")]
        //public virtual List<GiftShopCustomer> GiftShopCustomers { get; set; }

        [InverseProperty("Department")]
        public virtual List<Job> Jobs { get; set; }
    }
}
