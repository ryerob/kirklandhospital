﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KirklandHospital.Models.ViewModels
{
    public class SurveyQuestionEdit
    {
        public SurveyQuestionEdit()
        {

        }
        public virtual SurveyQuestion SurveyQuestion { get; set; }
    }

}