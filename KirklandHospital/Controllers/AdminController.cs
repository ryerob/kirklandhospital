﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using KirklandHospital.Models;
using KirklandHospital.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace KirklandHospital.Controllers
{
    public class AdminController : Controller
    {

        //Referenced Christine's code but typed it all out by hand to try and learn what was happening
        //Currently have the create function, edit function and show fuction. Still need delete
        private readonly KirklandDBContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public AdminController(KirklandDBContext context,  UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        // GET: Admins
        public async Task<ActionResult> Index()
        {
            var user = await GetCurrentUserAsync();
            if (user !=null)
            {
                if (user.AdminID == null) { ViewData["UserHadAdmin"] = "False"; }
                else { ViewData["UserHadAdmin"] = user.AdminID.ToString(); }
                return View(await db.Admins.ToListAsync());
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(await db.Admins.ToListAsync());
            }
        }

        public ActionResult Show(int id)
        {
            return RedirectToAction("Details/" + id);
        }

        public async Task<ActionResult> Details(int?id)
        {
            var user = await GetCurrentUserAsync();
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            Admin located_admin = await db.Admins.SingleOrDefaultAsync(a => a.AdminID == id);
            if (located_admin == null)
            {
                return NotFound();
            }
            if (user != null) {
                if(user.Admin == located_admin)
                {
                    ViewData["UserIsAdmin"] = "True";
                }
                else
                {
                    ViewData["UserIsAdmin"] = "False";
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False";
            }
            return View(located_admin);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("AdminID, AdminFName, AdminLName, AdminUsername, AdminRole")]Admin admin)
        {
            if (ModelState.IsValid)
            {
                db.Admins.Add(admin);
                //db.SaveChanges;
                var res = await MapUserToAdmin(admin); 

                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public async Task<ActionResult> Edit(int?id)
        {
            if(id==null)
            {
                return new StatusCodeResult(400);
            }
            Admin admin = db.Admins.Find(id);
            if(admin==null)
            {
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            if (user == null) { return Forbid(); }
            if(user.AdminID != id)
            {
                return Forbid();
            }
            return View(admin);
        }
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<ActionResult> Edit([Bind("AdminID, AdminFName, AdminLName, AdminUsernmae, AdminRole")]Admin admin)
        {
            var user = await GetCurrentUserAsync();
            if (user == null) { return Forbid(); }
            if(user.AdminID != admin.AdminID)
            {
                return Forbid();
            }
            if (ModelState.IsValid)
            {
                db.Entry(admin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details");
            }
            return View(admin);
        }

        private async Task<IActionResult> MapUserToAdmin(Admin admin)
        {
            var user = await GetCurrentUserAsync();
            user.Admin = admin;
            var user_res = await _userManager.UpdateAsync(user);
            if (user_res == IdentityResult.Success)
            {
                Debug.WriteLine("Success, mapped admin to user");
            }
            else
            {
                Debug.WriteLine("We did not map admin to user");
                return BadRequest(user_res);
            }
            admin.User = user;
            admin.UserID = user.Id;
            if (ModelState.IsValid)
            {
                db.Entry(admin).State = EntityState.Modified;
                var admin_res = await db.SaveChangesAsync();
                if (admin_res > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest(admin_res);
                }
            }
            else
            {
                return BadRequest("Unstable Admin Model.");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }


 }
    
