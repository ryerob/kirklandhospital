﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using KirklandHospital.Models;
using KirklandHospital.Data;
using System.Diagnostics;

namespace KirklandHospital.Controllers
{
    public class DonationController : Controller
    {
        private readonly KirklandDBContext db;

        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public DonationController(KirklandDBContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
          
            return View();
        }

        public async Task<ActionResult> List(int pagenum)
        {

            var user = await GetCurrentUserAsync();
            if(user== null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }

            var _donations = await db.Donations.ToListAsync();
            int donationcount = _donations.Count();
            int perpage = 8;
            int maxpage = (int)Math.Ceiling((decimal)donationcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<Donation> donations = await db.Donations.Skip(start).Take(perpage).ToListAsync();
            return View(donations);
        }
        [HttpPost]
        public ActionResult Create(string FirstName_new, string LastName_new, string Address_new, string City_new,
            string Country_new, string PostalCode_new, string Email_new, DateTime Date_new, string DonationAmount_new, string Comments_new,
            int RemainAnnon_new, Donation donation)
        {
            var current_date = DateTime.Now;
            donation.DonationDate = current_date;

            string query = "insert into Donations (DonationFName, DonationLName, DonationAddress, DonationCity," +
                "DonationCountry, DonationPostalCode, DonationEmail, DonationDate, DonationAmount, DonationComments, DonationAnon) " +
                "values (@fname, @lname, @address, @city, @country, @postalcode, @email, @date, @amount, @comments, @anon)";
            SqlParameter[] myparams = {
                new SqlParameter("@fname", FirstName_new),
                new SqlParameter("@lname", LastName_new),
                new SqlParameter("@address", Address_new),
                new SqlParameter("@city", City_new),
                new SqlParameter("@country", Country_new),
                new SqlParameter("@postalcode", PostalCode_new),
                new SqlParameter("@email", Email_new),
                new SqlParameter("@date", donation.DonationDate),
                new SqlParameter("@amount", DonationAmount_new),
                new SqlParameter("@comments", Comments_new),
                new SqlParameter("@anon", RemainAnnon_new)
            };

            db.Database.ExecuteSqlCommand(query, myparams);
            db.SaveChanges();
            //find the highest id in ecards to return the one that was just created for the show details page.
            //i know this is wrong but I can't find the sql_insert_id effective equivalent in ef core
            int newID = db.Donations.Max(d => d.DonationID);
            
            return RedirectToAction("ThankYou/" + newID);
        }

        public ActionResult ThankYou(int id)
        {

            Donation donation = db.Donations.SingleOrDefault(d => d.DonationID == id);


            return View(donation);
        }

        public async Task<ActionResult> Delete(int id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }
            string query = "delete from donations where DonationID = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }

        public async Task<ActionResult> Edit(int id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }
        
            Donation donationedit = new Donation();
            donationedit = db.Donations.Find(id);

            return View(donationedit);
        }

        [HttpPost]
        public ActionResult Edit(int? id, string FirstName, string LastName, string Address, string City,
            string Country, string PostalCode, string Email,  string DonationAmount, string Comments,
            int RemainAnnon)
        {
            if ((id == null) || (db.Donations.Find(id) == null))
            {
                return NotFound();
            }

            string query = "update Donations set DonationFName = @fname, DonationLName = @lname, DonationAddress = @address, " +
                "DonationCity = @city, DonationCountry = @country, DonationPostalCode = @postalcode, DonationEmail = @email, " +
                " DonationAmount = @amount, DonationComments = @comments, DonationAnon = @anon where DonationID = @id ";
            SqlParameter[] myparams = {
                new SqlParameter("@fname", FirstName),
                new SqlParameter("@lname", LastName),
                new SqlParameter("@address", Address),
                new SqlParameter("@city", City),
                new SqlParameter("@country", Country),
                new SqlParameter("@postalcode", PostalCode),
                new SqlParameter("@email", Email),
                new SqlParameter("@amount", DonationAmount),
                new SqlParameter("@comments", Comments),
                new SqlParameter("@anon", RemainAnnon),
                new SqlParameter("id", @id)
            };

            db.Database.ExecuteSqlCommand(query, myparams);
            return RedirectToAction("List");

        }

  
        public async Task<ActionResult> Show(int id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return BadRequest();
            }
            if (user.AdminID == null)
            {
                return Forbid();
            }
            Donation donation = new Donation();
            donation = db.Donations.Find(id);
            return View(donation);          
        }


    }
}