﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KirklandHospital.Models
{
    public class ECardTemplate
    {
        [Key]
        public int ECardTemplateID { get; set; }

        [Required, StringLength(150), Display(Name = "Title")]
        public string ECardTemplateTitle { get; set; }

        [Required]
        public string ECardTemplatePath { get; set; }

        [InverseProperty("ECardTemplate")]
        public List<ECard> ECards { get; set; }


    }
}
